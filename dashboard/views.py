from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from .forms import LoginForm
from django.contrib import messages


def user_login(request):
    if request.method == 'POST':
        # Utworzenie egzemplarza formularza wraz z wysłanymi danymi
        form = LoginForm(request.POST)

        # Spr. czy formularz jest prawidłowy (czy wszystkie pola zostały wypełnione)
        if form.is_valid():
            cd = form.cleaned_data

            """
            Jeżeli wysłane dane są prawidłowe, uwierzytelniamy użytkownika na podstawie informacji
            w bazie danych. Metoda authenticate() zwraca obiekt User, gdy użytkownik zostanie uwierzytelniony.
            
            WAŻNE: Metoda authenticate() sprawdza dane uwierzytelniające uzytkownika. Metoda login() 
            umieszcza użytkownika w bieżącej sesji.
            """
            user = authenticate(username=cd['username'], password=cd['password'])
            if user is not None:
                # Spr, czy konto użytkownika jest aktywne. Atrybut is_active pochodzi z modelu User.
                if user.is_active:
                    login(request, user)
                    messages.success(request, f"Zalogowano! Witaj, {cd['username']}.")
                    return redirect('dashboard')
                else:
                    messages.error(request, 'Twoje konto jest zablokowane')
                    return redirect('login')
            else:
                messages.error(request, 'Nieprawidłowe dane uwierzytelniające')
                return redirect('login')
    else:
        form = LoginForm()
    return render(request, 'dashboard/login.html', {'form': form})


def user_logout(request):
    logout(request)
    return render(request, 'dashboard/logout.html')


@login_required
def dashboard(request):
    return render(request, 'dashboard/dashboard.html')
